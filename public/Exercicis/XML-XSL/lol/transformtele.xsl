<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <cadena>
            <nom>Un TV</nom>
            <programas>
                <xsl:for-each select="./programacio/audiencia">
                    <program>
                        <xsl:attribute name="hora">
                            <xsl:value-of select="./hora"/>
                        </xsl:attribute>
                        <xsl:if test="./cadenes/cadena/@nom = 'Un TV'">
                            <nom-programa>
                                <xsl:value-of select="./../@percentatge"/>
                            </nom-programa>
                        </xsl:if>
                    </program>
                </xsl:for-each>
            </programas>
        </cadena>
    </xsl:template>
</xsl:stylesheet>
