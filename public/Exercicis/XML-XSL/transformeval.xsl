<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <style>
            table, tr, th {
        border : 1px solid black;
    }
            </style>
            <body>
                <table>
                    <tr>
                        <th>nombre</th>
                        <th>apellidos</th>
                        <th>telefono</th>
                        <th>repitdor</th>
                        <th>practicas</th>
                        <th>examen</th>
                        <th>final</th>
                    </tr>
                    <xsl:for-each select="./evaluacion/alumno">
                        <xsl:sort select="./apellidos" />
                        <tr>
                            <th>
                                <xsl:value-of select="./nombre"/>
                            </th>
                            <th>
                                <xsl:value-of select="./apellidos"/>
                            </th>
                            <th>
                                <xsl:value-of select="./telefono"/>
                            </th>
                            <th>
                                <xsl:value-of select="./@repite"/>
                            </th>
                            <th>
                                <xsl:value-of select="./notas/practicas"/>
                            </th>
                            <th>
                                <xsl:value-of select="./notas/examen"/>
                            </th>
                            <xsl:if test="(./notas/practicas + ./notas/examen)div2 &lt; 5">
                                <th style = "color : red">
                                    <xsl:value-of select='(./notas/practicas + ./notas/examen)div2'/>
                                </th>
                            </xsl:if>
                            <xsl:if test="(./notas/practicas + ./notas/examen)div2 &gt;=5 and (./notas/practicas + ./notas/examen)div2 &lt; 8">
                                <th>
                                    <xsl:value-of select='(./notas/practicas + ./notas/examen)div2'/>
                                </th>
                            </xsl:if>
                            <xsl:if test="(./notas/practicas + ./notas/examen)div2 &gt;= 8">
                                <th style = "color : blue">
                                    <xsl:value-of select='(./notas/practicas + ./notas/examen)div2'/>
                                </th>
                            </xsl:if>
                            <xsl:choose>
                                <xsl:when test='./foto'>
                                    <th>
                                        <img src="{concat(./nombre,./apellidos)}.png"/>
                                    </th>
                                </xsl:when>
                                <xsl:otherwise>
                                    <th>
                                        <img src="index.png"/>
                                    </th>
                                </xsl:otherwise>
                            </xsl:choose>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>